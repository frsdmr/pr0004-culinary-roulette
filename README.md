# pr004-culinary-roulette

* "what can I do"

    - www.alicefood.fun
    - say ...


* auto deploy on AWS       ( for using 80 & 443 port - need use root permissions )

    - create two folder in /home/ec2-user  (as ec2_user permissions !!!!):
        prj_dpl, prj_run

    - need copy tomcat-key (keystore.p12) to run folder (prj_run)

    - need copy the file (aws_runner.sh),
    - set file (aws_runner.sh) runnable and

    - start it as __root__ !!! (don't need use a cron, etc. )
      +----
      |# ./aws_runner.sh  > aws_runner.log &  (!!! & - as'daemon'  '>' -rewrite log !!!)

        ( when (pr).war file is placed to folder   prj_dpl
            - stop old running
            - move to new place
            - start it
        )


* secrets... 
    filling (from gitlab's CI/CD_Settings/Variables)
    - id_rsa.pem - stage deployToAWS (this .pem need for copying to ec2 ( used scp ) )
    - server.ssl.key-store-password in application.properties stage compile
    
    
* run on maven
    mvn spring-boot:run -Dspring-boot.run.arguments=--spring.config.name=application,--logging.config=logback.xml
    
#
