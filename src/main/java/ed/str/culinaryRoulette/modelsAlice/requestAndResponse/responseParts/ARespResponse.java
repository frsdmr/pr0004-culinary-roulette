package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.ARespResponseButton;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.ARespResponseCard;

import java.util.Arrays;

public class ARespResponse {

    public String text = " ARespResponse text ";
    public String tts  = "A-tts-Resp-Response text";
    public ARespResponseCard card ;//= new ARespResponseCard();
    //    public ARespResponseButton[] buttons = new ARespResponseButton[]{new ARespResponseButton("О Создателе","/some","TTS Some info about creator's")/*,new ARespResponseButton("Авторизация")*/};
    public ARespResponseButton[] buttons = new ARespResponseButton[]{new ARespResponseButton()/*,new ARespResponseButton("Авторизация")*/};
    public Boolean end_session = false;


    @Override
    public String toString() {
        return "{" +
                " \"text\" : \"" + text + '\"' +
                ", \"tts\" : \"" + tts + '\"' +
                (card != null ? ", \"card\" : " + card : "") +
                ", \"buttons\" : " + Arrays.toString(buttons) + " "+
                ", \"end_session\" :" + end_session +
                '}';
    }

}