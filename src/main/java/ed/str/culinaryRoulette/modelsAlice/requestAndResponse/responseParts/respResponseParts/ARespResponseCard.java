package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.respResponseCardParts.ARespResponseCardButton;

public class ARespResponseCard {


    @Override
    public String toString() {
        return "{" +
                " \"type\" : \"" + type + '\"' +
                ", \"image_id\" : \"" + image_id + '\"' +
                ", \"title\" : \"" + title + '\"' +
                ", \"description\" : \"" + description + '\"' +
                ", \"button\" : " + button +
                '}';
    }
    public String type = "BigImage";
    public String image_id = "";
    public String title = "";
    public String description = "";
    public  ARespResponseCardButton button = new ARespResponseCardButton();

}
