package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.reqAndRespParts;

public class AReqAndRespPayload {
    public String payloadText = null; // need _NULL_ ("" - error)

    @Override
    public String toString() {
        return "{" +
                " \"payloadText\" : \"" + payloadText + '\"' +
                '}';
    }

}
