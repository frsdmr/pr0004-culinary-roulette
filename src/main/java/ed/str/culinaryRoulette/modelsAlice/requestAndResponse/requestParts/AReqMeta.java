package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.reqRequestParts.reqRequestMetaParts.AReqMetaInterfaces;

public class AReqMeta {
    @Override
    public String toString() {
        return "{" +
                " \"client_id\" : \"" + client_id + '\"' +
                ", \"interfaces\" : " + interfaces  +
                ", \"locale\" : \"" + locale + '\"' +
                ", \"timezone\" : \"" + timezone + '\"' +
                '}';
    }

    public String client_id;
    public AReqMetaInterfaces interfaces;
    public String locale;
    public String timezone;
}
