package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AReqSession {
    @Override
    public String toString() {
        return "{" +
                " \"message_id\" : " + message_id +
                ", \"new\" : " + field_new +
                ", \"session_id\" : \"" + session_id + '\"' +
                ", \"skill_id\" : \"" + skill_id + '\"' +
                ", \"user_id\" : \"" + user_id + '\"' +
                '}';
    }

    public Integer message_id;// = 4;

    @JsonProperty("new")
    public Boolean field_new; // in REST we taking a field "new"

    public String session_id;// = "2eac4854-fce721f3-b845abba-20d60";
    public String skill_id;
    public String user_id;// = "E06901797A4FAC70921FF920BC0114E51A72B42110F7115133749FF7AAE89A8C";
}
