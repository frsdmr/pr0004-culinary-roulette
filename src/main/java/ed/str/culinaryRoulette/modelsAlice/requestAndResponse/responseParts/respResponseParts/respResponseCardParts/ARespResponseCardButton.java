package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.respResponseCardParts;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.ARespResponsePayload;

public class ARespResponseCardButton {

    public String text ="";
    public String url = "";
    public ARespResponsePayload payload = new ARespResponsePayload();


    @Override
    public String toString() {
        return "{" +
                " \"text\" : \"" + text + '\"' +
                ", \"url\" : \"" + url + '\"' +
                ", \"payload\" : " + payload +
                '}';
    }

}
