package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts;

import java.util.Date;

public class ARespResponseButton {

    public String title = " - ("+new Date()+") - ";
    public ARespResponsePayload payload = new ARespResponsePayload();
    public String url = null; // need null for disable "open url" process
    public Boolean hide = false;

    public ARespResponseButton() {
    }

    @Override
    public String toString() {
        return "{" +
                " \"title\" : \"" + title + '\"' +
                ", \"payload\" :" + payload +
                (url != null ? ", \"url\" : \"" + url + '\"' : "")  +
                ", \"hide\" : " + hide +
                '}';
    }

    public void setFields(String title, String url, String payloadText) {
        this.title  = title;
        this.url = url;
        this.payload.payloadText =  payloadText;
    }

}
