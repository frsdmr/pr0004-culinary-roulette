package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts;

public class ARespSession {
    @Override
    public String toString() {
        return "{" +
                " \"session_id\" : \"" + session_id + '\"' +
                ", \"message_id\" : " + message_id +
                ", \"skill_id\" : \"" + skill_id + '\"' +
                ", \"user_id\" : \"" + user_id + '\"' +
                '}';
    }
    public String session_id = "2eac4854-fce721f3-b845abba-20d60";
    public Integer message_id = 4;
    public String skill_id = "";
    public String user_id = "AC9WC3DF6FCE052E45A4566A48E6B7193774B84814CE49A922E163B8B29881DC";
}
