package ed.str.culinaryRoulette.modelsAlice.requestAndResponse;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.ARespResponse;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.ARespSession;


@SuppressWarnings("WeakerAccess")
public class ResponseToAlice {

    public ARespResponse response = new ARespResponse();
    public ARespSession session = new ARespSession();
    public String version = "1.0";


    @Override
    public String toString() {
        return "ResponseToAliceJSON {" +
                "\"response\" : " + response +
                ", \"session\" : " + session +
                ", \"version\" : \"" + version + '\"' +
                '}';
    }

}
