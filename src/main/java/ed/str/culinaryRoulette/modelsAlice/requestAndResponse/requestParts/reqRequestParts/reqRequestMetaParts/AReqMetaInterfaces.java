package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.reqRequestParts.reqRequestMetaParts;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.reqRequestParts.reqRequestMetaParts.reqRequestMetaInterfacesParts.AReqMetaInterfacesScreen;

public class AReqMetaInterfaces {
    @Override
    public String toString() {
        return (screen != null ? "{" +
                " \"screen\" : " + screen +
                '}' : "{}");
    }

    public AReqMetaInterfacesScreen screen;
}
