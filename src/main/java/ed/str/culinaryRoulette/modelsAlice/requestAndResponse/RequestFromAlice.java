package ed.str.culinaryRoulette.modelsAlice.requestAndResponse;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.AReqMeta;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.AReqRequest;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.AReqSession;

public class RequestFromAlice {
    @Override
    public String toString() {
        return "RequestFromAliceJSON {" +
                " \"meta\" : " + meta +
                ", \"request\" : " + request +
                ", \"session\" : " + session +
                ", \"version\" : \"" + version + '\"' +
                '}';
    }

    public AReqMeta meta = new AReqMeta();
    public AReqRequest request = new AReqRequest();
    public AReqSession session = new AReqSession();
    public String version;


}
