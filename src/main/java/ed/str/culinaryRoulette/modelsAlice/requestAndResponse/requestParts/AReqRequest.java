package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.reqRequestParts.AReqRequestNlu;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.reqRequestParts.AReqRequestPayload;


public class AReqRequest {

    public String command;
    public AReqRequestNlu nlu = new AReqRequestNlu();
    public String original_utterance;
    public AReqRequestPayload payload;
    public String type;

    @Override
    public String toString() {
        return "{" +
                (command != null ? " \"command\" : \"" + command + "\"," : "")  +
                " \"nlu\" : " + nlu + ","+
                (original_utterance != null ? " \"original_utterance\" : \"" + original_utterance + "\"," : "")  +
                (payload != null  ?  " \"payload\" : " + payload + ","  : "") +
                " \"type\" : \"" + type + "\"" +
                '}';
    }


}
