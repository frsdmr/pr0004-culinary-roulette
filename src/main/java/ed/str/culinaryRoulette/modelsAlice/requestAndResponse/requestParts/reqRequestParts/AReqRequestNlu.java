package ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.reqRequestParts;

import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.requestParts.reqRequestParts.AReqRequestNluParts.AReqRequestNluEntities;

import java.util.Arrays;

public class AReqRequestNlu {
    @Override
    public String toString() {
        return "{" +
                " \"entities\" : " + (entities.length == 0 ? "[]": Arrays.toString(entities) ) +""+
                ", \"tokens\" : "  + (tokens.length == 0 ? "[]" : "[\""+ String.join("\",\"",tokens) + "\"]")  +
                '}';
    }

    public AReqRequestNluEntities[] entities  = new AReqRequestNluEntities[]{};
    public String[] tokens = new String[]{};
}
