package ed.str.culinaryRoulette.repo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepoAuthGoogleUsers extends JpaRepository<AuthGoogleUser,String> {
}
