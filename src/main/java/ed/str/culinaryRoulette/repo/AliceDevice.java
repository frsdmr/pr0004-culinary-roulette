package ed.str.culinaryRoulette.repo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table( name = "alice_devices")
//@Data
public class AliceDevice {

    @Id
    @Getter
    @Setter
    private String aliceUserId;

    @Getter
    @Setter
    public LocalDateTime lastUseAliceDevice;

    @ManyToOne
    @JoinColumn(name = "sub_google_id_column")
    @Getter
    @Setter
    private AuthGoogleUser authGoogleUser;

    public AliceDevice() {
    }

    public AliceDevice(String aliceUserId) {
        this.aliceUserId = aliceUserId;
    }

    @Override
    public String toString() {
        return "AliceDev["+aliceUserId+"/"+lastUseAliceDevice+"/"+( authGoogleUser!=null? "["+authGoogleUser.subGoogleId +"/"+authGoogleUser.googleFullName+"]":"")+"]";
    }

}
