package ed.str.culinaryRoulette.repo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table( name = "google_users")
//@Data
public class AuthGoogleUser {

    public AuthGoogleUser() {
    }

    @Id
    // - - s1u2b
    @Getter
    @Setter
    public String subGoogleId;

    // - - name
    @Getter
    @Setter
    public String googleFullName;

//    @ToString.Exclude
//    @Transient                  // @Transient - exclude writing to base
//    //given_name
//    private String given_name;

//    @ToString.Exclude
//    @Transient
//    //family_name
//    private String family_name;

//    @ToString.Exclude
//    @Transient
//    //profile
//    private String profile;
//
//    @ToString.Exclude
//    @Transient
//    //picture
//    private String picture;

    //@ToString.Exclude
    //email
    @Getter
    @Setter
    public String email;

//    @ToString.Exclude
//    @Transient
//    //email_verified
//    private Boolean email_verified;

//    @ToString.Exclude
//    @Transient
//    //gender
//    private String gender;

//    @ToString.Exclude
//    @Transient
//    //locale
//    private String locale;


    @Getter
    @Setter
    public LocalDateTime lastLogin;

    @Getter
    @Setter
    public HashSet<String> roles;

    @OneToMany(mappedBy = "authGoogleUser")
    @Getter
    @Setter
    public Set<AliceDevice> aliceDevicesList = new HashSet<AliceDevice>();

    @Override
    public String toString() {
        return "AuthGoogleUser{" +
                "subGoogleId='" + subGoogleId + '\'' +
                ", googleFullName='" + googleFullName + '\'' +
                ", email='" + email + '\'' +
                ", lastLogin=" + lastLogin +
                ", roles=" + roles +
                ", aliceDevicesList=" + aliceDevicesList.stream().map(ad -> new String(ad.getAliceUserId())+"/"+new String(ad.getAuthGoogleUser().email)   ).collect(Collectors.joining(", ")) +
                "}<br>";
    }

}
