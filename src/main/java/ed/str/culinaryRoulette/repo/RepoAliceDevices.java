package ed.str.culinaryRoulette.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RepoAliceDevices extends JpaRepository<AliceDevice,String> {

    @Override
    Optional<AliceDevice> findById(String s);
}
