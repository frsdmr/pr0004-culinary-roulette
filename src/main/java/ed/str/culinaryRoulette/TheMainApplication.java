package ed.str.culinaryRoulette;

import ed.str.culinaryRoulette.repo.AliceDevice;
import ed.str.culinaryRoulette.repo.AuthGoogleUser;
import ed.str.culinaryRoulette.repo.RepoAliceDevices;
import ed.str.culinaryRoulette.repo.RepoAuthGoogleUsers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.net.ssl.*;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.Principal;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Supplier;

@EnableConfigurationProperties  // enabling descriptions (additional-spring-configuration-metadata.json) - additional properties in application.properties

@Controller
@SpringBootApplication

@EnableGlobalMethodSecurity(securedEnabled = true)          // this enable annotation like ( @Secured({"ROLE_ANOTHER","ROLE_ADM"}) )

@Configuration
@EnableWebSecurity

@EnableOAuth2Sso



public class TheMainApplication extends WebSecurityConfigurerAdapter
{

    private static Logger logger = LoggerFactory.getLogger(TheMainApplication.class);
    //Logger logger = LoggerFactory.getLogger("analytics"); // save log to another file (this property in logback.xml)


	@Override
	protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/wh","/yandex_4f2d534486f17935.html","/theSomeAbout","/gm","/js/lib/*","/geodata/*").permitAll();

		http.authorizeRequests()
				.antMatchers("/gru0").access("hasAnyRole('USER','ADM')")
				.antMatchers("/gru1").hasAnyRole("USER","ADM")
				.antMatchers("/gra","/actuator/shutdown").hasRole("ADM")
				.anyRequest().authenticated();

        http.csrf().disable();

        http.antMatcher("/**")
                .logout().logoutSuccessUrl("/login").permitAll();

		http                                                                // >>>>>   for password block auth start
				.formLogin()
				.loginPage("/login")                                        // to auth form
				.permitAll();                                               // <<<<<<  for password block auth finish

	}


    // >>>>>>>>  password form block start  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)	throws Exception {
        auth.inMemoryAuthentication().withUser("u").password("{noop}p").roles("USER");
        auth.inMemoryAuthentication().withUser("a").password("{noop}p").roles("ADM");
    }

    @GetMapping("/login")               // when  password auth
    public String login(Authentication authentication, Map<String, Object> model) {             // mapping to form
        model.put("authentication", authentication  );
        return "/WEB-INF/jsp/login.jsp";
    }
    // <<<<<<<<<<<<  password form block finish  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // >>>>>>>>  SSO  block start

    /*
    preparing Object for "Authentication" field "Principal"
    can get this formed Object -
    SecurityContextHolder.getContext().getAuthentication().getPrincipal();
     */
    @SuppressWarnings("Convert2Lambda")
    @Bean
    PrincipalExtractor principalExtractor( RepoAuthGoogleUsers repoAuthGoogleUsers ){
//        System.out.println("\n//1// \n"+userDetailsRepo        );

        return new PrincipalExtractor() {
            @Override
            public AuthGoogleUser extractPrincipal(Map<String, Object> map) {
                logger.debug("\n extractPrincipal \n >>>>>>> \n" + map + "\n");
                String id = (String) map.get("sub");

                AuthGoogleUser agUser = repoAuthGoogleUsers.findById(id).orElseGet(new Supplier<AuthGoogleUser>() {
                    @Override
                    public AuthGoogleUser get() {
                        AuthGoogleUser retAgUser = new AuthGoogleUser();

                        retAgUser.setSubGoogleId((String)map.get("sub"));
                        retAgUser.setGoogleFullName((String) map.get("name"));
                        retAgUser.setEmail((String)map.get("email"));

                        retAgUser.setRoles( new HashSet(Arrays.asList("ROLE_USER".split(","))) );
                        // frsdmr adm added
                        if("113766724164359578547".equals(retAgUser.getSubGoogleId()))
                            retAgUser.getRoles().add("ROLE_ADM");

                        return retAgUser;
                    }
                });
                agUser.setLastLogin(LocalDateTime.now());
                return repoAuthGoogleUsers.save(agUser);

//                return authGoogleUsers;
            }
        };

    }

    /*
    before any Request like
    @RequestMapping("/some")
    public User someUser(Principal pUser)
    This procedure fill "Granted Authorities" for "Authentication"
    it can get as  Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    or easy parameter of call (Authentication authentication)
    authentication.getAuthorities();
    (here set something like to - Granted Authorities: ROLE_USER, ROLE_ADM)
     */
    @SuppressWarnings("Convert2Lambda")
    @Bean
    AuthoritiesExtractor authoritiesExtractor ( RepoAuthGoogleUsers authGoogleUsers ){
//        System.out.println("\n//2// \n"+userDetailsRepo);

        return new AuthoritiesExtractor(){
            @Override
            public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
                AuthGoogleUser userFromDB = authGoogleUsers.findById((String) map.get("sub")).orElseThrow(null);

                //userFromDB.getRoles().add("ROLE_ADM");

                //noinspection SimplifyStreamApiCallChains
                return AuthorityUtils.createAuthorityList((String[]) userFromDB.getRoles().stream().toArray(String[]::new));
            }
        };
    }

    // logout // adding for standart [/logout] processing (standart - redirected to root and when (1)more than one logged user - request authorization (2)only one - automatic auth)
    @RequestMapping("/lgt") 
    public @ResponseBody String logout(HttpServletRequest request) {

        new SecurityContextLogoutHandler().logout(request, null, null);

        return "[ "+new Date()+" ] [ Logout. Bye... ]";
    }

    // <<<<<<<<<<<<<<<<  SSO  block finish

    @Autowired
    RepoAliceDevices aliceDevices;

    @Autowired
    public RepoAuthGoogleUsers authGoogleUsers;
//
//    public static ArrayList<AuthGoogleUser> gUsers;

    @RequestMapping("/")
    public String indJsp(/*RepoAuthGoogleUsers authGoogleUsers,*/ Authentication authentication, Map<String, Object> model) {
        model.put("authentication", authentication);

        model.put("authGoogleUsers", authGoogleUsers.findAll() );
        model.put("aliceDevices", aliceDevices.findAll());

//        User.allUsers = (ArrayList<User>) userDetailsRepo.findAll();
//        gUsers = (ArrayList) authGoogleUsers.findAll();

        return "/WEB-INF/jsp/index.jsp";
    }

    //
    // This static block need for make
    // !!! _HTTPS_ !!! POST or GET request to SELF SIGNED sites
    // (using HttpsURLConnection)
    //
    static {
        // this part is needed cause Lebocoin has invalid SSL certificate, that cannot be normally processed by Java
        TrustManager[] trustAllCertificates = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s)  {                    }
                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s)  {                    }
                    @Override
                    public X509Certificate[] getAcceptedIssuers() {                        return null;                    }
                }
        };

        @SuppressWarnings("Convert2Lambda")
        HostnameVerifier trustAllHostnames = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true; // Just allow them all.
            }
        };

        try {
            System.setProperty("jsse.enableSNIExtension", "false");
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCertificates, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(trustAllHostnames);
        } catch (GeneralSecurityException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    // @Secured({"ROLE_ANOTHER","ROLE_ADM"}) // !!! when __used oAuth__ !!! - annotations like this can be used only in another classes (in another file)
    @PostMapping("/shutdown")
    public @ResponseBody String down(HttpServletRequest request, @CookieValue(value = "JSESSIONID", defaultValue = "") String sessionId) throws IOException {

        URL https_url = new URL(request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath()) + "/actuator/shutdown");

        HttpsURLConnection con = (HttpsURLConnection) https_url.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "curl/7.45.0");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "*/*");
        con.setRequestProperty("Cookie", "JSESSIONID="+sessionId);

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        // wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + https_url);
        // System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return "[ "+new Date()+" ] ["+response+"] [ Down. Bye... ]";
    }


    @SuppressWarnings("Duplicates")
	@GetMapping("/gru0")
	public String gru0(Principal user, HttpServletRequest request, Authentication authentication,
						   @RequestParam(name="nam", required=false, defaultValue="some") String namex, @RequestParam(name="sec", required=false ) String sec, Model model) {
		model.addAttribute("name", namex);
		model.addAttribute("sec", sec);
		System.out.println("\n ***   gru0 !!!!!!!!! ");
		System.out.println(user);
		System.out.println(request);
		System.out.println(authentication);
		return "greet.jsp";
	}

	@SuppressWarnings({"Duplicates", "unused"})
	@RequestMapping("/gru1")
	public String gru1(Principal user, HttpServletRequest request, Authentication authentication,
							@RequestParam(name="nam", required=false, defaultValue="some") String namex, @RequestParam(name="sec", required=false ) String sec, Map<String, Object> model) {
		model.put("message","SHN HERE gru1");
		System.out.println("\n ***   gru1 !!!!!!!!! ["+TheMainApplication.class.toString()+"]" );
		System.out.println(user);
		System.out.println(request);
		System.out.println(authentication);

        logger.trace("doStuff needed more information - {}", namex);
        logger.debug("doStuff needed to debug - {}", namex);
        logger.info("doStuff took input - {}", namex);
        logger.warn("doStuff needed to warn - {}", namex);
        logger.error("doStuff encountered an error with value - {}", namex);

		return "greet.jsp";
	}


	@SuppressWarnings({"Duplicates", "unused"})
	@RequestMapping("/gra")
	public String greeting(Principal user, HttpServletRequest request, Authentication authentication,
						   @RequestParam(name="nam", required=false, defaultValue="some") String namex, @RequestParam(name="sec", required=false ) String sec, Map<String, Object> model) {
		model.put("message","SHN HERE gra");
//		model.addAttribute("sec", sec);
		System.out.println("\n ***   @RequestMapping(\"/gra\") ***");
		System.out.println(user);
		System.out.println(request);
		System.out.println(authentication);
		return "greet.jsp";
	}

    @RequestMapping("/gm")
    public String geomap(/*RepoAuthGoogleUsers authGoogleUsers,*/ Authentication authentication, Map<String, Object> model) {

        model.put("authentication", authentication);

        return "/WEB-INF/jsp/geomap.jsp";
    }

	public static void main(String[] args) {
		SpringApplication.run(TheMainApplication.class, args);
	}
}
