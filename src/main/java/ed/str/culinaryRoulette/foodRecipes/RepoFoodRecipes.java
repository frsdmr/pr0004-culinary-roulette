package ed.str.culinaryRoulette.foodRecipes;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RepoFoodRecipes  extends JpaRepository<FoodRecipe,String> {

    @Override
    Optional<FoodRecipe> findById(String s);

}
