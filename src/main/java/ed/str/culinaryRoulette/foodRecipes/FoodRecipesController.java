package ed.str.culinaryRoulette.foodRecipes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

@Controller
public class FoodRecipesController {

    private static Logger logger = LoggerFactory.getLogger(FoodRecipesController.class);

    @Autowired
    public RepoFoodRecipes repoFoodRecipes;


    @RequestMapping("/frListView")
    public String frListView(Authentication authentication)  {

        logger.debug("/frListView  ["+authentication+"]");

        return "/WEB-INF/jsp/foodRecipes.jsp";
    }

    // ----------------
    // DB Processing
    // ----------------
    @RequestMapping("/frListDB")
    public @ResponseBody ArrayList<FoodRecipe> frListDB()  {
        ArrayList<FoodRecipe> recipes = (ArrayList) repoFoodRecipes.findAll();
        recipes.sort(Comparator.comparing(FoodRecipe::getName));
        return recipes;
    }

    @Secured({"ROLE_ANOTHER","ROLE_ADM"})
    @PostMapping(value = "/foodRecipesSaveRecipe")
    public @ResponseBody FoodRecipe foodRecipesSaveRecipe( @RequestBody (required=false) FoodRecipe recipe)  {

        repoFoodRecipes.save(recipe);
        return recipe;
    }

    @Secured({"ROLE_ANOTHER","ROLE_ADM"})
    @PostMapping(value = "/foodRecipesDeleteRecipe")
    public @ResponseBody FoodRecipe foodRecipesDeleteRecipe( @RequestBody (required=false) FoodRecipe recipe)  {

        repoFoodRecipes.delete(repoFoodRecipes.findById(recipe.getFoodRecipeId()).get());
        return recipe;
    }

    // ------------------
    // END DB Processing
    // ------------------

    @PostMapping(value="/frupload")
    public @ResponseBody String handleFileUpload( @RequestParam("file") MultipartFile file){

        String name = "---";

        if (!file.isEmpty()) {
            try {
                name = file.getOriginalFilename();
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(name)));
                stream.write(bytes);
                stream.close();
                return "Вы удачно загрузили " + name + " в " + name + "-uploaded !";
            } catch (Exception e) {
                return "Вам не удалось загрузить " + name + " => " + e.getMessage();
            }
        } else {
            return "Вам не удалось загрузить " + name + " потому что файл пустой.";
        }
    }
}
