package ed.str.culinaryRoulette.foodRecipes;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "food_recipes")
@Data

public class FoodRecipe {

    @Id
    private String foodRecipeId;

    private String shortName;

    private String name;
    private String tts;

    // description's

    // steps (table with foodRecipeId and steps's description)


    //private String profile_file;

}
