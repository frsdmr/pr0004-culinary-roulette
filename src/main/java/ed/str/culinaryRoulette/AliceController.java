package ed.str.culinaryRoulette;


import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.RequestFromAlice;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.ResponseToAlice;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.ARespResponseButton;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.ARespResponseCard;
import ed.str.culinaryRoulette.modelsAlice.requestAndResponse.responseParts.respResponseParts.respResponseCardParts.ARespResponseCardButton;
import ed.str.culinaryRoulette.repo.AliceDevice;
import ed.str.culinaryRoulette.repo.AuthGoogleUser;
import ed.str.culinaryRoulette.repo.RepoAliceDevices;
import ed.str.culinaryRoulette.repo.RepoAuthGoogleUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

@RestController
public class AliceController {

//    static ArrayList sessions

    private RequestFromAlice requestFromAlice;

    @SuppressWarnings("SpellCheckingInspection")
    private ArrayList<String> recipes = new ArrayList<>();
    {
        recipes.add("Готовим колбаску.");
        recipes.add("Жарим картошку с курицей.");
        recipes.add("Варим пельмени.");
        recipes.add("Тушим печенку.");
        recipes.add("Запекаем картошку.");
        recipes.add("Просто заварим доширак.");
        //recipes.add("Пива нажраться - очень плохая идея ?");
        recipes.add("Лаваш, жареное, а потом порезаное мясо, с помидорой, луком, немного огурца, майонез, кетчуп, соль, перец - офигенная шаверма...");
        //recipes.add("Просто открой холодильник, и возьми, то что лежит ближе всего.");
        recipes.add("Варим раков.");
        recipes.add("Готовим креветки.");
        recipes.add("Вкусный средиземноморский суп. Для средиземноморского антуража можно добавить морепродуктов.");
        recipes.add("Жарим вареную (русскую) колбаску.");
        recipes.add("Надо купить шейку, нашпиговать чесноком, можно еще немного помариновать, а потом просто запечь.");
        recipes.add("Помидорно луково майонезный салат. Лука побольше, соль и перец по вкусу.");

    }

//    @SuppressWarnings("SpellCheckingInspection")
//    private ArrayList<String> additionalTip = new ArrayList<>();
//    {
//        additionalTip.add("Мойте руки перед едой.");
//        additionalTip.add("Капля никотина убивает лошадь...");
//        additionalTip.add("А вообще - Ешь вода пей вода - какать не будешь никада.");
//        additionalTip.add("надо выходить из за стола с чувством легкого недоедания.");
//        additionalTip.add("Ну не знаю, насколько красное вино полезно для пищеварения...");
//        additionalTip.add("Кошачий сок - то же самое, что и томатный, но только из кошек...");
//        additionalTip.add("Согласие есть продукт непротивления сторон...");
//        additionalTip.add("Инетересно, а вы знаете, кто такой вуглускр ?");
//        additionalTip.add("А лучше - просто не открывай холодильник, и ничего не бери.");
//        additionalTip.add("Не забывайте закусывать...");
//        additionalTip.add("Наверное к дождю...");
//    }

//    ..Ну я не знаю, что ты хочешь... самое вкусное наверное жареная колбаска а не
@SuppressWarnings("SpellCheckingInspection")
private ArrayList<String> iDontKnowPrefList = new ArrayList<>();
    {
        //iDontKnowPrefList.add("По пожеланиям Алексея сейчас я ничего не скажу про колбаску, а тем более жареную. ");
        //iDontKnowPrefList.add("Ну я не знаю, что ты хочешь... Алексей, извините, но самое вкусное наверное жареная колбаска а не - ");
        //iDontKnowPrefList.add("Чудесное время суток. Прекрасно что вы болтаете со мной.");
        //iDontKnowPrefList.add("Попробуем с хохляцким акцентом. Еще раз здоровеньки булы.");
        iDontKnowPrefList.add("Не совсем понятна фраза...");
        //iDontKnowPrefList.add("Пива нет. к чему это я... наверное к тому, что не уловила сути...");
//        iDontKnowPrefList.add("");
    }

    // some said phrase
    //

    @SuppressWarnings({"SpellCheckingInspection", "MismatchedQueryAndUpdateOfCollection"})
    private ArrayList<String> iDontKnowSuffList = new ArrayList<>();

    {
        iDontKnowSuffList.add("...не совсем понятно для меня...");
        iDontKnowSuffList.add("...я конечно поняла, но не совсем врубилась про что ты.");
        iDontKnowSuffList.add("... может ты сказал что то не то.");
        iDontKnowSuffList.add("... интересно, к чему бы все это.");
        iDontKnowSuffList.add("... но может это и к лучшему.");
        iDontKnowSuffList.add("... возможно не все так грустно.");
//        iDontKnowSuffList.add("");
    }

    @Value( "${alice.wh.url:https://localhost/}" )
    private String aliceWhUrl;

    @Autowired                                  // !!! repo WILL BE Autowired !!! Not a parameter of function
    RepoAliceDevices aliceDevices;

    @Autowired
    RepoAuthGoogleUsers authGoogleUsers;

//    @PostMapping("/wh")
    @RequestMapping("/wh")
    public ResponseToAlice wh(
            @RequestBody(required = false) RequestFromAlice ar,
            /*, @RequestParam(name="aliceSessionUserId", required=false, defaultValue = "") String aliceSessionUserId*/
            @RequestParam(name="debTokens", required=false /*, defaultValue = ""*/) String[] debTokens
    ){


        ResponseToAlice resp = new ResponseToAlice();

        resp.response.buttons[0].title += " \n SomeAbout - О создателе...";
        resp.response.buttons[0].url = aliceWhUrl+"/theSomeAbout" ;
        resp.response.buttons[0].payload.payloadText = "Show about info process";


        // check for param in url _debTokens_ (may be array) if present - set AR "tokens" for testing request processing)
        if(debTokens != null && debTokens.length != 0){
            ar = new RequestFromAlice();
            ar.session.user_id = "E06901797A4  like this  id   E51A72B42110F7115133749FF7AAE89A8C";
            ar.request.nlu.tokens = debTokens;
            ar.session.field_new = false;
        }

        if (ar == null) return resp;

        requestFromAlice = ar;



        if( "ping".equals(requestFromAlice.request.command) ) {
            System.out.println("\n\n >>>> ping >>>> ("+new Date()+") >>>>> ["+requestFromAlice.session.session_id+"]\n  ["+requestFromAlice.session.user_id+"]");
        } else {
            System.out.println("\n\n >>>> " + requestFromAlice);
        }


        //        if (!auth) show auth button
        AliceDevice aliceDev = aliceDevices.findById(requestFromAlice.session.user_id).orElseGet(new Supplier<AliceDevice>() {
            @Override
            public AliceDevice get() {
                AliceDevice retAliceDev = new AliceDevice(requestFromAlice.session.user_id);

                aliceDevices.save(retAliceDev);

                return retAliceDev;
            }
        });
        aliceDev.setLastUseAliceDevice(LocalDateTime.now());
        aliceDevices.save(aliceDev);

        ArrayList<ARespResponseButton> aliceRespArrListOfButtons = new ArrayList<> (Arrays.asList(resp.response.buttons) );

        ARespResponseButton authButton = new ARespResponseButton();
        authButton.title = "authButton";
        authButton.url = aliceWhUrl+"/wh/aliceSessionUserId/"+requestFromAlice.session.user_id;
        authButton.payload.payloadText = "Authentication process";
//        authButton.payload = resp.response.new ARespResponsePayload();
        // to payload tts about login
        aliceRespArrListOfButtons.add(0,authButton);

        ARespResponseButton anotherButton = new ARespResponseButton();
        anotherButton.title = "другой случайный вариант еды";
        anotherButton.url = null; //  not "";   // !!!!
        anotherButton.hide = true;
        anotherButton.payload.payloadText = "Start Rotate Rulette";// = resp.response.new ARespResponsePayload();
        aliceRespArrListOfButtons.add(anotherButton);


        //noinspection ToArrayCallWithZeroLengthArrayArgument
        resp.response.buttons =  aliceRespArrListOfButtons.toArray( new  ARespResponseButton[aliceRespArrListOfButtons.size()] );

        if (aliceDev.getAuthGoogleUser() != null){

            //auth
            System.out.println(">>>>&^&^^%$^>>>>> "+aliceDev.getAuthGoogleUser().getGoogleFullName());

            // add button logout
            ARespResponseButton butt = resp.response.buttons[0];

            butt.title = " ["+aliceDev.getAuthGoogleUser().getGoogleFullName()+"] DeAuth";
            butt.url = aliceWhUrl+"/wh/deAuthAliceSessionUserId/"+requestFromAlice.session.user_id;
            authButton.payload.payloadText = "DeAuthentication process";

        }


        // fields
        resp.session.message_id = requestFromAlice.session.message_id;
        resp.session.session_id = requestFromAlice.session.session_id;
        resp.session.skill_id = requestFromAlice.session.skill_id;
        resp.session.user_id = requestFromAlice.session.user_id;

        // logic here
        if(requestFromAlice.request.nlu.tokens.length == 1 && "ping".equals(requestFromAlice.request.nlu.tokens[0]) ){
            resp.response.text = "pong";
            resp.response.tts = resp.response.text;
        }
        else if( requestFromAlice.session.field_new ){
            resp.response.text = /*"Рады наблюдать вас здесь... Тут возможны недопустимые для вас слова, предложения и даже мысли... ну и содержимое может быть 18+. Если Вас что то не устраивает, то просто скажите до свидания..."*/
                    "Это кулинарная рулетка.\n Случайный вариант еды. \n\n "+ getRandomString(recipes)+ " . \n\n Вы также можете сказать, например следующее:\n - \"может что то другое\", или\n - \"что бы приготовить\", \n" +
                            "и Вам будет предложен какой-нибудь другой вариант чего-бы вы могли приготовить и поесть." +
                            "\n Более развернутые рецепты будут здесь обязательно. \nНо позже... пока не сделано... :( ...";
            resp.response.tts  =  resp.response.text;//"Хорошего Вам дня. " + additionalTip.get((int) ((double) (Math.random() * additionalTip.size()))) + resp.response.text ;
            // активационные имена можно проверять здесь при новой сессии в !!! original_utterance !!! т.к.  tokens=[]
            System.out.println("\n==== (initial) >>>> \n "+requestFromAlice.request.original_utterance);


            //add speach (for speaking after processing )

        }
        else if( "ButtonPressed".equals(requestFromAlice.request.type) ){
            System.out.println("\n\n>>>>>> ButtonPressed ["+requestFromAlice+"]\n\n");

            resp.response.text = ( requestFromAlice.request.payload != null && requestFromAlice.request.payload.payloadText != null ? requestFromAlice.request.payload.payloadText : "Processing : Button was Pressed...");
            resp.response.tts = resp.response.text;

            if ("Start Rotate Rulette".equals( requestFromAlice.request.payload != null ? requestFromAlice.request.payload.payloadText : "Processing : Button was Pressed...")){
                String randomRecept = getRandomString(recipes);
//            String randomTip = getRandomString(additionalTip);
                resp.response.text = "Крутим рулетку... \n\n" + randomRecept /*+"  "+ randomTip*/;
                resp.response.tts = "Крутим рулетку... - - - <speaker audio=\"alice-sounds-game-loss-1.opus\"> - - - ... " + randomRecept /*+"  "+ randomTip*/;

            }
        }



        else if ( allTokensIsInThisWordList(new ArrayList<>(asList("пока","досвидания","счастливо","покедова","увидимся", "bye", "до", "свидания")))    ){
            resp.response.text = "Досвидания...";
            resp.response.tts = "Good Bye";
            resp.response.end_session = true;
        }

        else if ( allTokensIsInThisWordList(new ArrayList<>(asList("помощь","help", "хелп")))   ){
            resp.response.text = "вы можете сказать , например - \"может что то другое\", или - \"что бы приготовить\", или нажать кнопку (\"другой случайный вариант еды\"), и Вам будет предложен какой-нибудь другой случайный рецепт. ";
            resp.response.tts = resp.response.text;
        }

        else if ( allTokensIsInThisWordList(new ArrayList<>(asList("что","ты","умеешь","а")))   ){
            resp.response.text = "я могу предложить случайный рецепт для готовки. и озвучу следующий случайный рецепт, если Вы скажете - \"может что то другое\", или - \"что бы приготовить\", или нажмете кнопку (\"другой случайный вариант еды\")";
            resp.response.tts = resp.response.text;
        }

        else if (allTokensIsInThisWordList(new ArrayList<>(asList("super","Вау", "сиськи","просто","обалденные","обалденная","грудь","восхитительная","суперская","суперский","восхитительно","супер","тити","сисяндры","титьки","шикарная","грудь","роскошная",""))) ){

            resp.response.text = "Это конечно не еда... но не заметить это нельзя...";
            resp.response.tts = "Это конечно не еда... но не заметить это нельзя...";
            //set picture

            if ( requestFromAlice.meta.interfaces != null ){                   // check for alice device without screen
                resp.response.card = new ARespResponseCard();
                resp.response.card.image_id ="213044/0c326a3080567ce38874";
                resp.response.card.title = "Просто шикарно";
                resp.response.card.description = "Tits Descriptions";

                resp.response.card.button = new ARespResponseCardButton();
                resp.response.card.button.text = "Это вам не батоны...";
                resp.response.card.button.url = null;
                resp.response.card.button.payload.payloadText = " some for buttons text ";
            }

        }

        else if (allTokensIsInThisWordList(new ArrayList<>(asList("другой","случайный","вариант","еды",   "давай","а","ну", "может", "что", "то", "ты", "еще", "нибудь", "ченить", "другое", "чего", "что", "бы", "приготовить","чегобы","чтобы","чтото","скажи","другой","рецепт","на","ужин","завтрак","обед","пожрать","слопать","схомячить","","","","","","",""))) ){
            String randomRecept = getRandomString(recipes);
//            String randomTip = getRandomString(additionalTip);
            resp.response.text = "Крутим рулетку... \n\n" + randomRecept /*+"  "+ randomTip*/;
            resp.response.tts = "Крутим рулетку... - - - <speaker audio=\"alice-sounds-game-loss-1.opus\"> --- ... " + randomRecept /*+"  "+ randomTip*/;
        }

        else if (allTokensIsInThisWordList(new ArrayList<>(asList("темка","артем","артемчик","темчик","артемий","арти","темочка","тибозик","пусик","лапусик","балбесик","артемка"))) ){
            String randomRecept = getRandomString(recipes);
//            String randomTip = getRandomString(additionalTip);
            resp.response.text = "Артемка привет ! ";
            resp.response.tts = "Артемка привет ! <speaker audio=\"alice-sounds-game-loss-1.opus\">";
        }

        else if (allTokensIsInThisWordList(new ArrayList<>(asList("иди","спать","артемчик","темчик","артемий","арти","темочка","тибозик","пусик","лапусик","балбесик","артемчик"))) ){
            String randomRecept = getRandomString(recipes);
//            String randomTip = getRandomString(additionalTip);
            resp.response.text = "Артемка иди спать ! ";
            resp.response.tts = "Артемка иди спать ! <speaker audio=\"alice-sounds-game-loss-1.opus\">";
        }

        else {

//            System.out.println("\n[SESSION'S]\n"+xx);

            resp.response.text = getRandomString(iDontKnowPrefList) + " " +requestFromAlice.request.command /*+ getRandomString(iDontKnowSuffList)*//* + " " + getRandomString(additionalTip) */+"... но давайте предложу - " +getRandomString(recipes);
            resp.response.tts = resp.response.text;
        }

        if( "pong".equals(resp.response.text) ) System.out.println(" <<<< pong <<<< ("+new Date()+") <<<<<"); else
        System.out.println("\n <<<< " + resp);

        return resp;
    }

    @GetMapping("/wh/deAuthAliceSessionUserId/{aliceSessionUserIdValue}")
    public String deAuthReqToBrowser(Authentication authentication, @PathVariable(name= "aliceSessionUserIdValue", required=false) String aliceSessionUserIdValue){

        AliceDevice aliceDevice = aliceDevices.findById(aliceSessionUserIdValue).get();
        aliceDevice.setAuthGoogleUser(null);
        aliceDevices.save(aliceDevice);

        return "" + aliceDevice;
    }


    @GetMapping("/wh/aliceSessionUserId/{aliceSessionUserIdValue}")
    public String authReqToBrowser ( Authentication authentication, @PathVariable(name= "aliceSessionUserIdValue", required=false) String aliceSessionUserIdValue){

        System.out.println("---- ONE ---\n"+ aliceDevices.findAll());

        AliceDevice aliceDevice = aliceDevices.findById(aliceSessionUserIdValue).get();

        AuthGoogleUser googleUser = authGoogleUsers.findById(((AuthGoogleUser) authentication.getPrincipal()).subGoogleId).get();

        googleUser.getAliceDevicesList().add(aliceDevice);
        aliceDevice.setAuthGoogleUser(googleUser);

        aliceDevices.save(aliceDevice);

//        authGoogleUsers.save(googleUser);

        System.out.println("---- TWO ---\n"+ aliceDevices.findAll());

        return "\n------\n["+aliceDevice+"]<br><br>\n["+googleUser+"]";
    }

    private String getRandomString(ArrayList<String> additionalTip) {
        return additionalTip.get((int) ((double) (Math.random() * additionalTip.size())));
    }

    private boolean allTokensIsInThisWordList(ArrayList words) {

        for (int i = 0; i < requestFromAlice.request.nlu.tokens.length; i++){
            if ( !words.contains(requestFromAlice.request.nlu.tokens[i])   ) return false;
        }

        return requestFromAlice.request.nlu.tokens.length != 0;
    }




}
