<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: e.streltsov
  Date: 01.04.2019
  Time: 12:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title GeoMap VI</title>

    <style>
        .map {
            height: 600px;
            width: 100%;
        }
    </style>

    <%--<link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css" type="text/css">--%>
    <link rel="stylesheet" href="/js/lib/openlayer-en-5-3-0.css" type="text/css">

    <%--<script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>--%>
    <script src="/js/lib/openlayer-en-5-3-0.js"></script>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.2.1/proj4.js"></script>--%>
    <script src="/js/lib/proj4js-en-2-2-1.js"></script>


    <%--
    (https://developer.tech.yandex.ru/services/3/ ) yandex Personal Cabinet - statistic & api_key for using it
    <script src="https://api-maps.yandex.ru/2.1/?apikey=  < API KEY HERE>   &lang=ru_RU" type="text/javascript"></script>
    now used downloaded and placed to _js/lib_
    --%>
    <script src='/js/lib/api-maps-yandex-ru-2-1.js'></script>



</head>
<body>
<%--<p>--%>
<p>
    ${authentication}
<p>
    ***
<p>
        <%= new java.util.Date() %>
<%--<p>--%>
<p>
    ${authGoogleUsers}
<p><br>
    <%--<%= User.allUsers %>--%>
    <%--<%= TheMainApplication.gUsers %>--%>
    <%--<br>--%>
    <%--x - alice devices--%>
<p><br>
    <c:forEach items="${aliceDevices}" var="item">
        ${item}<br>
    </c:forEach>

    <%--y   type="text/javascript"  --%>
<p><br>

<h2>OL</h2>
<div id="mapOL" class="map"></div>
<script>

    // ------------------  for YANDEX Layer ( work with _5_.x library too ) projection converter -----------------------------------

    proj4.defs("EPSG:3395","+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs");
    ol.proj.proj4.register(proj4); // 5.x lib
    // ol.proj.get('EPSG:3395').setExtent(
    //     [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244]
    // );   // устанавливается или здесь или в tileGrid слоя яндекса

    var ed_cad_num_sa = '63:17:405012:155';

    var ed_cad_num_ld = '63:26:1907014:11';

    var map = new ol.Map({
        target: 'mapOL',
        layers: [

            // ---------------------------  Base Map ('s)  ------------------------------

            // new ol.layer.Tile({
            //     source: new ol.source.OSM()
            // }),                                          //      THE  OSM

            // new ol.layer.Tile({
            //     source: new ol.source.Stamen({
            //         layer: 'watercolor'                 // watercolor,  toner, terrain(need check)
            //     })
            // }),


            new ol.layer.Tile({
                // opacity: 0.6,
                // extent: [ 2047800.24508882355, 5586452.183179816, 2847800.24508882355, 6386452.183179816 ],  // границы отображения
                source: new ol.source.XYZ({
                    url: 'https://vec0{1-4}.maps.yandex.net/tiles?l=map&x={x}&y={y}&z={z}',
                    projection: 'EPSG:3395',
                    tileGrid: ol.tilegrid.createXYZ({
                        extent: [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244]
                    })
                })
            }),


            // =================================   Base maps from RR    =============================
            // new ol.layer.Tile({                              // **** !!! PPK5  ( RReestr )   (  layers information )  !!! ***
            //     source: new ol.source.TileWMS({
            //         url: 'https://pkk5.rosreestr.ru/arcgis/services/Cadastre/CadastreWMS/MapServer/WMSServer',
            //         params: {'LAYERS': '33,32,31,30,29,28,27,25,24,23,22,21,20,19,18,16,15,14,12,11,10,9,8,6,5,4,3,2,1','TILED': true}
            //         })
            // }),
            //
            // // RR базовый слой карты
            // new ol.layer.Tile({
            //     source: new ol.source.XYZ({
            //         // ratio: 1,
            //         url: 'https://pkk5.rosreestr.ru/arcgis/rest/services/BaseMaps/BaseMap/MapServer/tile/{z}/{y}/{x}',
            //         params: {'TRANSPARENT':'true'}
            //     })
            // }),
            //
            // // RR подписи (улицы и т.п.)
            // new ol.layer.Tile({
            //     source: new ol.source.XYZ({
            //         // ratio: 1,
            //         url: 'https://pkk5.rosreestr.ru/arcgis/rest/services/BaseMaps/Anno/MapServer/tile/{z}/{y}/{x}',
            //         params: {'TRANSPARENT':'true'}
            //     })
            // }),


            // ---------------------------  vector's  ------------------------------
            // new ol.layer.Vector({
            //     opacity: 0.4,
            //     source: new ol.source.Vector({
            //         format: new ol.format.GeoJSON(),
            //         url: '/geodata/countries.geojson',
            //         params: {'TRANSPARENT':'true'}
            //     })
            // }),

            // ---------------------------  additional layers's  ( RR cadastre objects )  ------------------------------
            //
            //  taking info from RR about (center: [ 5566806.390105069,  7009835.059495612 ]) for cadastre object
            //      (https://ru.scribd.com/document/367583217/Pkk5-Services) - здесь описание
            //
            //  (https://pkk5.rosreestr.ru/api/features/1/63:17:405012:155) - получение детальной информации по cn
            //
            //   (https://pkk5.rosreestr.ru/api/features/1?text=63:26:1907014:11) - предварительный поиск для для уточнения cn & id
            //
            //

            new ol.layer.Tile({
                opacity: 0.6,
                source: new ol.source.TileWMS({
                    url: 'https://pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/CadastreSelected/MapServer/export',
                    params: {'layers': 'show:6,7' , 'f': 'image' , 'layerDefs': '{"6":"ID=\''+ed_cad_num_sa+'\'","7":"ID=\''+ed_cad_num_sa+'\'"}' }
                })
            }),

            // new ol.layer.Tile({
            //     opacity: 0.6,
            //     source: new ol.source.TileWMS({
            //         url: 'https://pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/CadastreSelected/MapServer/export',
            //         params: {'layers': 'show:6,7' , 'f': 'image' , 'layerDefs': '{"6":"ID=\'63:26:0:4192\'","7":"ID=\'63:26:0:4192\'"}'}
            //     })
            // }),

            new ol.layer.Tile({
                opacity: 0.6,
                source: new ol.source.TileWMS({
                    url: 'https://pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/CadastreSelected/MapServer/export',
                    params: {'layers': 'show:6,7' , 'f': 'image' , 'layerDefs': '{"6":"ID=\'' + ed_cad_num_ld + '\'","7":"ID=\'' + ed_cad_num_ld + '\'"}' }
                })
            })

        ],
        view: new ol.View({
            // center: ol.proj.fromLonLat([37.573856, 55.751574]),  //     !!!!   в ОЛ сначала (Lon)Долгота потом (Lat)Широта  !!!!!! (а у гугла и яндекса - широта/долгота)  55°45'04.7"N 37°34'26.1"E  ( 55.751306, 37.573906 ) (this center moscow)
            // center: ol.proj.fromLonLat([ 50.1902, 53.1994 ]),    //  (this is smr office)
            center: [ 5566806.390105069,  7009835.059495612 ],
            zoom: 17
        })
    });

</script>


<h2>YAM</h2>
<div id="mapYA" class="map"></div>
<script type="text/javascript">
    // Функция ymaps.ready() будет вызвана, когда
    // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
    ymaps.ready(init);

    function init(){
        // Создание карты.
        var myMap ;
        myMap = new ymaps.Map("mapYA", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [53.1994, 50.1902],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 7
        });

        myMap.behaviors.disable('scrollZoom');

        myMap.controls.add("zoomControl", {
            position: {top: 15, left: 15}
        });

        var myPlacemark = new ymaps.Placemark([53.1994, 50.1902] , {},
            { iconLayout: 'default#image',
                iconImageHref: 'https://ae01.alicdn.com/kf/HTB1kn9unnnI8KJjy0Ffq6AdoVXaD.jpg',
                iconImageSize: [25, 25],
                iconImageOffset: [-20, -47] });

        myMap.geoObjects.add(myPlacemark);

    }
</script>

■■■
<%--<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A034705bb051c877a85be97908ffd6f7bd1ea3f0c3456e370cce973fe2f0927c6&amp;width=500&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>--%>

</body>
</html>
