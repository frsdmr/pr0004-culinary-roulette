<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: e.streltsov
  Date: 28.11.2018
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html ng-app="foodRecipies" ng-controller="foodRecipiesCtrl" ng-init="firstName= 'John7';some(321);" lang="en">


<head>
    <meta charset="utf-8">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="/js/lib/angular.min.js"></script>


    <title>Food Recipes</title>
</head>

<body>
Food Recipes
<br><br>

<%= new java.util.Date() %> [{{theCurrentTime}}] <br> <br>

<ul>
    <li ng-repeat="recipe in recipies">

        <sec:authorize access="hasAnyRole('ROLE_ADM', 'ROLE_DEVELOPER')">
            <detailed-recipe ng-click="foodRecipesFormRecipeEdit.clickEdit(recipe)" recipe="recipe"></detailed-recipe><span ng-click="foodRecipesFormRecipeEdit.clickDelete(recipe)">X</span>
        </sec:authorize>

        <sec:authorize access="not hasAnyRole('ROLE_ADM', 'ROLE_DEVELOPER')">
            <detailed-recipe recipe="recipe"/>
        </sec:authorize>

    </li>
</ul>

<%--admin block--%>
<sec:authorize access="hasAnyRole('ROLE_ADM', 'ROLE_DEVELOPER')">

    <h3>ADMIN Edit Block HERE</h3>


    <div ng-if="!foodRecipesFormRecipeEdit.isShow">
        <button ng-click="foodRecipesFormRecipeEdit.clickEdit()"> ADD New </button>
    </div>

    <div ng-if="foodRecipesFormRecipeEdit.isShow">
        <p><input type="text" ng-model="foodRecipesFormRecipeEdit.shortName"></p>
        <p><input type="text" ng-model="foodRecipesFormRecipeEdit.name"></p>
        <p><input type="text" ng-model="foodRecipesFormRecipeEdit.tts"></p>
        <button ng-click="foodRecipesFormRecipeEdit.clickSubmit()"> SUBMIT </button>
        <button ng-click="foodRecipesFormRecipeEdit.clickCancel()"> CANCEL </button>
    </div>

    <br><hr>

    <h4>Upload recipies</h4>
    <form method="POST" enctype="multipart/form-data" action="frupload">
        File to upload: <input type="file" name="file" onchange="angular.element(this).scope().fileNameChanged()"><br />
        <br />
        <input type="submit"  value="Upload"> Press here to upload the file!
    </form>

</sec:authorize>


</body>

<script src="/js/foodRecipes.js"></script>
<script src="/js/components/detailed-recipe/detailed-recipe.component.js"></script>


</html>
