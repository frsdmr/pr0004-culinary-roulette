
// Define the `foodRecipies` module
foodRecipies = angular.module('foodRecipies', [
    // ...which depends on the `detailedRecipe` module
    'detailedRecipeModule'
]);

foodRecipies
.controller('foodRecipiesCtrl', function($scope, $location, $interval, $http) {

    window.$scope = $scope;
    window.$location = $location;

    // set model view and variables

    $scope.recipies = [];

    $scope.foodRecipesFormRecipeEdit = {
        isShow : false,
        foodRecipeId: "",
        shortName   : "",
        name        : "",
        tts         : "",
        clickEdit   :   {},
        clickDelete :   {},
        clickSubmit :   {},
        clickCancel :   {}
    };

    // -------------------------------
    // common and initial function's
    // load from db
    // -------------------------------

    $scope.loadFoodRecipesFromDB = function(){
        $http.get("frListDB").then(
            function(response){
                $scope.recipies = response.data;
            },
            function(response){
                console.log("-");
                console.log(response);
            });
    };


    var initial = function(){
        console.log('***34567**');

        $scope.theCurrentTime = new Date().toLocaleTimeString();
        $interval(function () {
            $scope.theCurrentTime = new Date().toLocaleTimeString();
        }, 1000);

        $scope.loadFoodRecipesFromDB();

    };



    // -------------------------
    // end initial function
    // -------------------------

    initial();

    // end initial
    //-----------------------------------------


    // function's
    // foodRecipesFormRecipeEdit
    $scope.foodRecipesFormRecipeEdit.clickEdit = function(recipe){
        if (recipe == undefined){
            $scope.foodRecipesFormRecipeEdit.foodRecipeId = "frId_"+new Date().getTime();
            $scope.foodRecipesFormRecipeEdit.shortName = "uno";
            $scope.foodRecipesFormRecipeEdit.name = "dos";
            $scope.foodRecipesFormRecipeEdit.tts = "tres";
        }else{
            $scope.foodRecipesFormRecipeEdit.foodRecipeId = recipe.foodRecipeId;
            $scope.foodRecipesFormRecipeEdit.shortName = recipe.shortName;
            $scope.foodRecipesFormRecipeEdit.name = recipe.name;
            $scope.foodRecipesFormRecipeEdit.tts = recipe.tts;
        }
        $scope.foodRecipesFormRecipeEdit.isShow = true;
    };

    $scope.foodRecipesFormRecipeEdit.clickDelete = function(recipe){
        $http.post("foodRecipesDeleteRecipe", recipe).then(
            function(response){
                $scope.foodRecipesFormRecipeEdit.isShow = false;
                $scope.loadFoodRecipesFromDB();
            },
            function(response){
                console.log("-");
                console.log(response);
            });
    };

    $scope.foodRecipesFormRecipeEdit.clickSubmit = function(){
        var recipe = {};
        recipe.foodRecipeId = $scope.foodRecipesFormRecipeEdit.foodRecipeId ;
        recipe.shortName = $scope.foodRecipesFormRecipeEdit.shortName;
        recipe.name = $scope.foodRecipesFormRecipeEdit.name;
        recipe.tts = $scope.foodRecipesFormRecipeEdit.tts;

        $http.post("foodRecipesSaveRecipe", recipe).then(
            function(response){
                $scope.foodRecipesFormRecipeEdit.isShow = false;
                $scope.loadFoodRecipesFromDB();
            },
            function(response){
                console.log("-");
                console.log(response);
            });
    };

    $scope.foodRecipesFormRecipeEdit.clickCancel = function(){
        $scope.foodRecipesFormRecipeEdit.isShow = false;
    };


});