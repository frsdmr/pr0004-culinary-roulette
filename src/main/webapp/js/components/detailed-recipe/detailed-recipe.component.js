
// Define the `detailedRecipeModule` module
//
// in html
// used TAG
//
// <detailed-recipe recipe="recipe"></detailed-recipe>
//
// Connecting: used
//
// <script src="/js/components/detailed-recpe/detailed-recipe.component.js"></script>
//

// part of descriptor
angular.module('detailedRecipeModule', []);

// part of implementation
angular.
module('detailedRecipeModule').
component('detailedRecipe', {

    template: '[{{$ctrl.recipe.foodRecipeId}}] [{{$ctrl.recipe.shortName}}] [{{$ctrl.recipe.name}}] [{{$ctrl.recipe.tts}}]   {{$ctrl.recipe.description}}',

    bindings: {
        recipe: '='
    }
});
